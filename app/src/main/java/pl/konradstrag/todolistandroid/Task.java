package pl.konradstrag.todolistandroid;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;

public class Task {

    public String id = "";
    public String name = "";
    public String description = "";
    public String date = "";

    public void load(SharedPreferences sp, String id) throws JSONException {
        String jsonString = sp.getString(id,"");
        if("" == jsonString){
            return;
        }

        ArrayList<String> stringArray = new ArrayList<String>();
        JSONArray jsonArray = new JSONArray(jsonString);

        this.id = id;
        this.name = jsonArray.getString(0);
        this.description = jsonArray.getString(1);
        this.date = jsonArray.getString(2);

    }

    @SuppressLint("CommitPrefEdits")
    public void save(SharedPreferences sp, String id) throws JSONException {
        String data[] = { this.name, this.description, this. date };
        JSONArray jsonArray = new JSONArray(data);
        sp.edit().putString(id, jsonArray.toString() ).commit();
    }

}
