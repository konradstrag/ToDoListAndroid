package pl.konradstrag.todolistandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import org.json.JSONException;

public class AddTaskActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    public void save(){
        Task t = new Task();
        t.name = "name";
        t.date = "2018-06-10";
        t.description = "description\nasd";
        try {
            t.save(this.sharedPreferences, "task_id_0");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("task_id", 0);
        startActivity(intent);

        this.finish();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);
        this.sharedPreferences = this.getSharedPreferences("pl.konradstrag.todolist", MODE_PRIVATE);
        this.load();
    }

    public void save(View view) {
        String id = "";
        int count_of_tasks = sharedPreferences.getInt(MainActivity.COUNT_OF_TASK,0);
        if(getIntent().hasExtra("task_id")){
            id = getIntent().getParcelableExtra("task_id");
        }else{
            count_of_tasks++;
            id = MainActivity.TASK_ID_PREFIX + count_of_tasks;
        }
        this.save();
        sharedPreferences.edit().putInt(MainActivity.COUNT_OF_TASK, count_of_tasks).commit();
    }

    public void load(){
        String id = "";
        if(getIntent().hasExtra("task_id")){
            id = getIntent().getParcelableExtra("task_id");
        }

        Task t = new Task();
        try {
            t.load(this.sharedPreferences, id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        EditText editText1 = (EditText) findViewById(R.id.name_box);
        EditText editText2 = (EditText) findViewById(R.id.date_box);
        EditText editText3 = (EditText) findViewById(R.id.description_box);
        editText1.setText(t.name);
        editText2.setText(t.date);
        editText3.setText(t.description);

    }
}
